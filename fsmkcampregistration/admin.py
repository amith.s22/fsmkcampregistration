from django.contrib import admin
from django.db import models
from .models import Registration

class RegistrationAdmin(admin.ModelAdmin):
    list_display = ['name','email','is_paid','track','region']
    search_fields = ('email','mobile','name')
admin.site.register(Registration, RegistrationAdmin)
