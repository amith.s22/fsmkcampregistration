from django.db import models
from django.contrib.auth.models import User

class Registration(models.Model):

    tracks = (
        ('RoR','Ruby On Rails'),
        ('And','Android'),
        ('IoT','Internet of Things')
    )
    region = (
        ('mandya','Mandya'),
        ('bangalore','Bengaluru'),
        ('mangalore','Mangaluru'),
        ('hassan','Hassan'),
        ('others','Others')
    )
    tshirt = (
        ('s','S'),
        ('m','M'),
        ('l','L'),
        ('xl','XL'),
        ('xxl','XXL')
    )
    laptop = (
        ('y','Yes')
        ('n','No')
        ('m','Maybe')
    )
    name = models.CharField(max_length=200,verbose_name="Full Name")
    college = models.CharField(max_length=300,verbose_name="College Name")
    year = models.IntegerField(verbose_name="Year of Study")
    dept = models.CharField(max_length=5,verbose_name="Department")
    email = models.EmailField(verbose_name="Participant Email")
    mobile = models.PositiveIntegerField(verbose_name="Participant Contact Number")
    track = models.CharField(max_length=5,verbose_name="Track Registering For",choices=tracks)
    region = models.CharField(max_length=20,verbose_name="Region of \
                              Karnataka",choices=region)
    tshirt = models.CharField(max_length=20,verbose_name="Tshirt Size",choices=tshirt , blank=True, null=True,default='s')
    laptop = models.CharField(max_length=20,verbose_name="Getting Laptop?",choices=laptop ,default='y')
    is_paid = models.BooleanField(default=False,verbose_name="Has the participant paid ?")
    payment_receiver = models.CharField(max_length=200,verbose_name="Who received the payment")
    coordinator = models.ForeignKey(User,verbose_name="Who is the coordinator",
                                   on_delete=models.PROTECT)
    transaction_id = models.CharField(max_length=200,verbose_name="FSMK \
                                      transaction ID", blank=True, null=True)

    def __str__(self):
        return self.email

    class Meta:
        ordering = ['-track',]
